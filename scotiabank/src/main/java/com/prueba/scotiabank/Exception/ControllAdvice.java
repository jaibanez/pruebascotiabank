/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.scotiabank.Exception;

import com.prueba.scotiabank.Wrapper.ResponseMsg;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 *
 * @author Jeison Ibañez
 */
@RestControllerAdvice
public class ControllAdvice {

    @ExceptionHandler(CustomNullPointerException.class)
    public ResponseMsg handleNotFoundException(CustomNullPointerException ex) {
        ResponseMsg responseMsg = new ResponseMsg(ex.getMessage());
        return responseMsg;
    }
}
