/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.scotiabank.Wrapper;

/**
 *
 * @author Jeison Ibañez
 */
public class ResponseWrapper {
    
    private int longitud;
    private int letras;
    private int caracteres_especiales;
    private int espacios;
    private int signos;
    private int numeros;

    public ResponseWrapper() {
    }

    public ResponseWrapper(int longitud, int letras, int caracteres_especiales, int espacios, int signos, int numeros) {
        this.longitud = longitud;
        this.letras = letras;
        this.caracteres_especiales = caracteres_especiales;
        this.espacios = espacios;
        this.signos = signos;
        this.numeros = numeros;
    }
    
    
    /**
     * @return the longitud
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the letras
     */
    public int getLetras() {
        return letras;
    }

    /**
     * @param letras the letras to set
     */
    public void setLetras(int letras) {
        this.letras = letras;
    }

    /**
     * @return the caracteres_especiales
     */
    public int getCaracteres_especiales() {
        return caracteres_especiales;
    }

    /**
     * @param caracteres_especiales the caracteres_especiales to set
     */
    public void setCaracteres_especiales(int caracteres_especiales) {
        this.caracteres_especiales = caracteres_especiales;
    }

    /**
     * @return the espacios
     */
    public int getEspacios() {
        return espacios;
    }

    /**
     * @param espacios the espacios to set
     */
    public void setEspacios(int espacios) {
        this.espacios = espacios;
    }

    /**
     * @return the signos
     */
    public int getSignos() {
        return signos;
    }

    /**
     * @param signos the signos to set
     */
    public void setSignos(int signos) {
        this.signos = signos;
    }

    /**
     * @return the numeros
     */
    public int getNumeros() {
        return numeros;
    }

    /**
     * @param numeros the numeros to set
     */
    public void setNumeros(int numeros) {
        this.numeros = numeros;
    }
    
    
}
