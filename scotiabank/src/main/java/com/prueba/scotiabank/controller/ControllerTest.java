/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.scotiabank.controller;

import com.prueba.scotiabank.Wrapper.RequestWrapper;
import com.prueba.scotiabank.Wrapper.ResponseWrapper;
import org.springframework.stereotype.Controller;

/**
 *
 * @author Jeison Ibañez
 */
@Controller
public class ControllerTest {

    public ResponseWrapper analize(RequestWrapper requestWrapper) throws Exception{
        
        ResponseWrapper response = new ResponseWrapper();

        String message = requestWrapper.getMensaje().toUpperCase();

        char[] arrayChar = message.toCharArray();

        for (int i = 0; i < arrayChar.length; i++) {
            char dato = arrayChar[i];
            int ascii = (int) dato;

            if (ascii >= 65 && ascii <= 90) {
                response.setLetras(response.getLetras()+1);
            } else if (ascii >= 48 && ascii <= 57) {
                response.setNumeros(response.getNumeros()+1);
            } else if (ascii == 32) {
                response.setEspacios(response.getEspacios()+1);
            } else if (ascii >= 33 && ascii <= 47
                    || ascii >= 58 && ascii <= 64
                    || ascii >= 91 && ascii <= 96
                    || ascii >= 123 && ascii <= 126) {
                response.setSignos(response.getSignos()+1);
            } else if (ascii >= 128) {
                response.setCaracteres_especiales(response.getCaracteres_especiales()+1);
            }
        }

        //Aca se calcula la longitud
        response.setLongitud(message.length());

        return response;
    }
    
    
}
