/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.scotiabank.Exception;

/**
 *
 * @author Jeison Ibañez
 */
public class CustomNullPointerException extends RuntimeException {

    public CustomNullPointerException(String msg) {
        super(msg);
    }

}
