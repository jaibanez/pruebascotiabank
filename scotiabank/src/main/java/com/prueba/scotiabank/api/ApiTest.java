/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.prueba.scotiabank.api;

import com.prueba.scotiabank.Exception.CustomNullPointerException;
import com.prueba.scotiabank.Wrapper.RequestWrapper;
import com.prueba.scotiabank.Wrapper.ResponseWrapper;
import com.prueba.scotiabank.controller.ControllerTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Jeison Ibañez
 */
@RestController
@RequestMapping("v1/prueba")
public class ApiTest {

    @Autowired
    private ControllerTest controllerTest;

    @PostMapping("/")
    public ResponseEntity<?> analize(@RequestBody RequestWrapper requestWrapper) {

        try {

            ResponseWrapper response = this.controllerTest.analize(requestWrapper);

            return new ResponseEntity<>(response, HttpStatus.OK);
            
        } catch (Exception c) {
            throw new CustomNullPointerException("Mensaje personalizado "+c.toString());
        }

    }
}
